package main

import (
	"log"
	"net/http"
	"time"
)

func main() {
	err := initConfig()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Starting micropub server...")
	log.Println("Current time: " + time.Now().Format(time.RFC3339))
	log.Println("Blog URL: " + BlogUrl)
	http.HandleFunc("/micropub", HandleMicroPub)
	http.HandleFunc("/media", HandleMedia)
	log.Fatal(http.ListenAndServe(":5555", nil))
}
