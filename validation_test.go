package main

import (
	"testing"
)

func TestGetContentType(t *testing.T) {
	for key, value := range map[string]ContentType{
		"":                                  UnsupportedType,
		"test":                              UnsupportedType,
		"application/x-www-form-urlencoded": WwwForm,
		"application/json":                  Json,
		"multipart/form-data":               Multipart,
	} {
		t.Run(key, func(t *testing.T) {
			got, err := GetContentType(key)
			if got != value {
				t.Errorf("Wrong ContentType")
			}
			if value == UnsupportedType && err == nil {
				t.Errorf("Error is nil")
			}
		})
	}
}
