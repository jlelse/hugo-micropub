FROM golang:1.15-alpine3.12 as build
ADD . /app
WORKDIR /app
RUN go build

FROM alpine:3.12
RUN apk add --no-cache tzdata ca-certificates
COPY --from=build /app/hugo-micropub /bin/
EXPOSE 5555
CMD ["hugo-micropub"]