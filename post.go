package main

import (
	"bytes"
	"errors"

	"gopkg.in/yaml.v3"
)

type HugoFrontMatter struct {
	Title          string                  `yaml:"title,omitempty"`
	Published      string                  `yaml:"date,omitempty"`
	Updated        string                  `yaml:"lastmod,omitempty"`
	Tags           []string                `yaml:"tags,omitempty"`
	Series         []string                `yaml:"series,omitempty"`
	ExternalURL    string                  `yaml:"externalURL,omitempty"`
	IndieWeb       HugoFrontMatterIndieWeb `yaml:"indieweb,omitempty"`
	Syndicate      []string                `yaml:"syndicate,omitempty"`
	TranslationKey string                  `yaml:"translationKey,omitempty"`
	Images         []string                `yaml:"images,omitempty"`
	Audio          string                  `yaml:"audio,omitempty"`
}

type HugoFrontMatterIndieWeb struct {
	Reply HugoFrontMatterReply `yaml:"reply,omitempty"`
	Like  HugoFrontMatterLike  `yaml:"like,omitempty"`
}

type HugoFrontMatterReply struct {
	Link  string `yaml:"link,omitempty"`
	Title string `yaml:"title,omitempty"`
}

type HugoFrontMatterLike struct {
	Link  string `yaml:"link,omitempty"`
	Title string `yaml:"title,omitempty"`
}

func writeFrontMatter(entry *Entry) (string, error) {
	frontMatter := &HugoFrontMatter{
		Title:       entry.title,
		Published:   entry.date,
		Updated:     entry.lastmod,
		Tags:        entry.tags,
		Series:      entry.series,
		ExternalURL: entry.link,
		IndieWeb: HugoFrontMatterIndieWeb{
			Reply: HugoFrontMatterReply{
				Link:  entry.replyLink,
				Title: entry.replyTitle,
			},
			Like: HugoFrontMatterLike{
				Link:  entry.likeLink,
				Title: entry.likeTitle,
			},
		},
		Syndicate:      entry.syndicate,
		TranslationKey: entry.translationKey,
		Audio:          entry.audio,
	}
	for _, image := range entry.images {
		frontMatter.Images = append(frontMatter.Images, image.url)
	}
	writer := new(bytes.Buffer)
	writer.WriteString("---\n")
	err := yaml.NewEncoder(writer).Encode(&frontMatter)
	if err != nil {
		return "", errors.New("failed encoding frontmatter")
	}
	writer.WriteString("---\n")
	return writer.String(), nil
}

func WriteHugoPost(entry *Entry) (string, error) {
	buff := new(bytes.Buffer)
	f, err := writeFrontMatter(entry)
	if err != nil {
		return "", err
	}
	buff.WriteString(f)
	buff.WriteString(entry.content)
	return buff.String(), nil
}
