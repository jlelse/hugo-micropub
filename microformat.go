package main

type MicroformatItem struct {
	Type       []string               `json:"type"`
	Properties *MicroformatProperties `json:"properties"`
}

type MicroformatProperties struct {
	Name          []string      `json:"name,omitempty"`
	Published     []string      `json:"published,omitempty"`
	Updated       []string      `json:"updated,omitempty"`
	Category      []string      `json:"category,omitempty"`
	Content       []string      `json:"content,omitempty"`
	Url           []string      `json:"url,omitempty"`
	InReplyTo     []string      `json:"in-reply-to,omitempty"`
	LikeOf        []string      `json:"like-of,omitempty"`
	BookmarkOf    []string      `json:"bookmark-of,omitempty"`
	MpSlug        []string      `json:"mp-slug,omitempty"`
	MpSyndicateTo []string      `json:"mp-syndicate-to,omitempty"`
	Photo         []interface{} `json:"photo,omitempty"`
	Audio         []string      `json:"audio,omitempty"`
}